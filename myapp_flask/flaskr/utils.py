import pickle
import os

from sklearn import preprocessing
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
import lightgbm as lgb

# Interpretability of the predictions
import lime
import lime.lime_tabular

from flaskr.models import Content


def predict_content(content):
    """Predict the probaibility that input belong to given classes
    Parameters:
        content: selected row of  pandas dataframe
    returns:
        json object with the probibliy array, the explanation of the prediction carried out with lime, 
        and the class names"""
    
    basedir = os.path.abspath(os.path.dirname('utils.py'))
        
    # Load the models from pickle file
    pkl_filename = "pickle_output.pkl"
    #with open(pkl_filename, 'rb') as file:
    with open(os.path.join(basedir, 'flaskr', 'model', pkl_filename), 'rb') as file:
        output = pickle.load(file)
    
    # load the object stored in the pikle file
    scaler = output['scaler']
    imputer = output['imputer']
    columns = output['columns']
    lr_model = output['model']
    X_train = output['X_train']
    #class_names = output['class_names']
    
    feature_names = list(columns.columns)
    class_names = ['0', '1']
    
    # Make sure the data are aligned identically with the ones used to train the model
    # Align the two df
    content, _ = content.align(columns, join='right', axis=1)
    
    # Transform to the Content class
    content = Content(content)
    
    # Apply the proprocess function
    #X = content.preprocess(imputer=None, scaler=None)
    X = content.preprocess(imputer=imputer, scaler=scaler)
    
    # Predict the result
    #pred = lr_model.predict_proba(X)
    
    # load the lime expaliner object
    explainer = lime.lime_tabular.LimeTabularExplainer(X_train, feature_names=feature_names, class_names=class_names, kernel_width=3)
    
    # interpretation of the prediction
    exp = explainer.explain_instance(X[0], lr_model.predict_proba, num_features=6)
    exp_dict = dict(exp.as_list())
    
    pred = exp.predict_proba
    
    result = {'class names': class_names,
              'probability': pred.tolist(),  # json does not handle a numpy object
              'explaination': exp_dict}
    return result
