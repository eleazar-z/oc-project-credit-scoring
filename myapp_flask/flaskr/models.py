import numpy as np

class Content(object):
    """Class Content that take a pandas dataframe in input and return a numpy array. The class contains its preprocess method"""
    
    def __init__(self, value):
        self.array = value.values
        
    def preprocess(self, imputer=None, scaler=None):
        """Apply some preprocessing functions such imputing null values and standardizing the data before predicting the content"""
        X = self.array
        if imputer is not None:
            X = imputer.transform(X)
        if scaler is not None:
            X = scaler.transform(X)
            
        return X