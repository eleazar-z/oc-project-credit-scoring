import functools
from flask import Flask, jsonify, request, render_template
from flask import flash, jsonify
import pandas as pd
from flaskr.utils import *

app = Flask(__name__)

# a simple page page that says hello
@app.route('/')
def hello():
	return 'Hello, World!'
    

@app.route('/predict/', methods=['POST'])
def predict_input():
    """Returns json HTML POST request. Input is json dict"""
    
    # request input
    input_json = request.data.decode()
    
    # convert the json input to a pandas dataframe
    content_value = pd.read_json(input_json, orient='index')
    
    # apply the prediction function imported from utils
    result = predict_content(content_value)
    
    # return the result in json format
    return jsonify({'result': result})
