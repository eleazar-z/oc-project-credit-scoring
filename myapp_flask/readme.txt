This file describes the flask-api app

The modelling results have been deployed on heroku
Project Layout --
- Profile
- requirements.txt
- run.py
- runtime.txt
- flaskr
    - __init__.py
    - views.py
    - model.py
    - utils.py
    - model
        - pickle_output.pkl
- setup.py
- readme.txt

to run in a development mode with debugger
$ cd ./myapp_flask
$ export FLASK_APP=flaskr
$ export FLASK_ENV=development
$ flask run
and visit 'http://127.0.0.1:5000/' in a browser

APP deployed on heroku
url = 'https://oc-credit-scoring.herokuapp.com/' (root page, hello world)
url = 'https://oc-credit-scoring.herokuapp.com/predict/' (curl request in bash)
