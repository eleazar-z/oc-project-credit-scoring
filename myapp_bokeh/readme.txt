This file describes the dashboard project and its layout

It has been carried out for a data science training program on OpenClassrooms (credit scoring modeling).

This program uses bokeh library and builds a dashboard that displays the prediction result and its interpretation, and the descriptive values of the different entries.

The dashboard provides five tabs.

Tha dashboard has been deployed on heroku

url = 'https://oc-dashboard.herokuapp.com/myapp_bokeh'

To load the program on a local server, download the folder, set the top-level directory and run 'bokeh serve myapp_bokeh' and open 'http://localhost:5006/myapp_bokeh' in a browser.

Project layout(myapp_bokeh):
- myapp_bokeh
    - data
        - sample_test.csv
    - models
        - pickle_output.pkl
    - scripts
        - histogram.py
        - predict_api.py
        - predict_local.py
        - radar.py
        - scatter.py
        - table.py
    - main.py
- Procfile
- requirements.txt
- runtime.txt
- readme.txt