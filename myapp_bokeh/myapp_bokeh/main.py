# main file for the bokeh dashboard app

# standard python packages
import pandas as pd
import numpy as np
import pickle
# os methods for manipulating paths
import os

# Bokeh basics
from bokeh.io import curdoc
from bokeh.models.widgets import Tabs


# Tabs drawn by the scripts
from scripts.scatter import scatter_tab
from scripts.table import table_tab
from scripts.histogram import histogram_tab
from scripts.radar import radar_tab
from scripts.predict_api import predict_tab
#from scripts.predict_local import predict_tab_2

# Read the df
basedir = os.path.abspath(os.path.dirname('main.py'))
df = pd.read_csv(os.path.join(basedir, 'myapp_bokeh', 'data', 'sample_test.csv'), sep=',')

# select limited features to draw the dashboard
selected_columns = ['AMT_ANNUITY', 'AMT_CREDIT', 'AMT_INCOME_TOTAL', 'CNT_CHILDREN', 'DAYS_BIRTH', 
                    'DAYS_EMPLOYED', 'EXT_SOURCE_2', 'TARGET', 'SK_ID_CURR']

# Load the pikle file with the model outputs
# uncomment to use predict_tab_2
#pkl_filename = "pickle_output.pkl"
#with open(os.path.join(basedir,'myapp_bokeh', 'models', pkl_filename), 'rb') as file:
#    pkl_model = pickle.load(file)

# create the different tabs
# first tab for the predicted score results in tab1 (forthcoming)
tab1 = predict_tab(df)
#tab1b = predict_tab_2(df, pkl_model)
tab2 = radar_tab(df, selected_columns)  # radar plot
tab3 = table_tab(df, selected_columns)  # decritive statistics
tab4 = histogram_tab(df, selected_columns)  # histogram plot
tab5 = scatter_tab(df, selected_columns)   # scatter plot


# Pull all the tabs into one application
#tabs = Tabs(tabs=[tab1b, tab2, tab3, tab4, tab5])
tabs = Tabs(tabs=[tab1, tab2, tab3, tab4, tab5])

# Put the tabs in the current document for display
curdoc().add_root(tabs)