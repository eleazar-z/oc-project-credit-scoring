## scatter plot script
import pandas as pd
import numpy as np

from bokeh.plotting import figure
from bokeh.models import Select, Panel, PreText, ColumnDataSource
from bokeh.layouts import row, column
from bokeh.palettes import Spectral5

def scatter_tab(df, selected_features):
    """Plot a scatter plot of two numeric columns
    Parameters:
        df: pandas dataframe
        selected_features: list
    Returns:
        tab: bokeh tab"""
    # copy the df and use in the function
    # select only numeric columns
    data = df[selected_features].copy()
    
    # Transform number of days in year
    #data['DAYS_EMPLOYED'] = df['DAYS_EMPLOYED']/-365
    data['DAYS_BIRTH'] = df['DAYS_BIRTH']/-365
    
    # SELECT colors for hover
    COLORS = Spectral5
    N_COLORS = len(COLORS)
    
    def make_dataset(data, x, y, c):
        """create the data source for the plot
        Parameters:
            data: pandas df
            x, y: strings, selected column names
            c: string, grouping column
        Returns: 
            bokeh ColumnDataSource"""
        
        if x=='SK_ID_CURR':
            xs = np.arange(1, len(data)+1)
        else:
            xs = data[x]
            
        if y =='SK_ID_CURR':
            ys = np.arange(1, len(data)+1)
        else:
            ys = data[y]
        
        # Create the grouping colors (default color is "#31AADE" when None)
        color = ["#31AADE"]*data.shape[0]
        if c !='None':
            if len(set(data[c])) > N_COLORS:
                groups = pd.qcut(data[c].values, N_COLORS, duplicates='drop')
            else:
                groups = pd.Categorical(data[c])
                
            color = [COLORS[elt] for elt in groups.codes]
            
        return ColumnDataSource({'xs':xs, 'ys':ys, 'color':color})
    
    def make_plot(src):
        """Draw a scatter plot
        Parameters:
            src: bokeh ColumnDataSource
        Returns:
            p: bokeh figure object"""
        tools = 'pan,box_zoom,hover,reset'
        p = figure(plot_height=600, plot_width=800, tools=tools)
        p.xaxis.axis_label = 'X-axis'
        p.yaxis.axis_label = 'Y-axis'
        p.title.text = 'Scatter plot'
        p.circle(x='xs', y='ys', color='color', line_color='white',size=10, source=src)
        return p
    
    def update_plot(attr, old, new):
        """update the plot according to the new value of x and y"""
        x_new = x.value
        y_new = y.value
        c_new = c.value
        p.xaxis.axis_label = x_new
        p.yaxis.axis_label = y_new
        p.title.text = 'Scatter plot of {} and {}'.format(x_new, y_new)
        src_new = make_dataset(data, x_new, y_new, c_new)
        src.data.update(src_new.data)
        #layout.children[1] = create_figure(scr_new)
        
    # initialize the data source and the figure
    src = make_dataset(data, x='SK_ID_CURR', y='AMT_CREDIT', c='None')
    p = make_plot(src)
    
    # create controls for call-back selection of x and y
    range_select = sorted(data.columns)
        # X-axis select (x)
    x = Select(title='X-axis', value='SK_ID_CURR', options=range_select)
    x.on_change('value', update_plot)
        # Y-axis select (y)
    y = Select(title='Y-axis', value='AMT_CREDIT', options=range_select)
    y.on_change('value', update_plot)
        # color select (c)
    c = Select(title='Color', value='None', options=['None']+range_select)
    c.on_change('value', update_plot)
        # put controls in a single column
    controls = column(x, y, c, width=200)
    
    # create a layout
    layout = row(controls, p)
    
    # Cretate a title for the tab
    tab_title = PreText(text='Scatter plot',style={'font-size':'18pt', 'font-weight': 'bold'})
    
    # Make a tab with the layout
    tab = Panel(child=column(tab_title, layout), title='scatter')
    
    return tab