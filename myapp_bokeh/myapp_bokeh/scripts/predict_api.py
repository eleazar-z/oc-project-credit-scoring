# prediction and interpretation tab script
# This script uses the API to predict the outcome

# basic python libraries
import numpy as np
import pandas as pd

# bokeh libraries
from bokeh.models import LabelSet, ColumnDataSource
from bokeh.plotting import figure
from bokeh.io import curdoc
from bokeh.layouts import row, column
from bokeh.models import Select, Panel, PreText

# html request on the api
import requests

def predict_tab(data):
    """Draw horizontals bars of the predicted probability and the interpretation of the output (contribution of some features) based on provided imput
    Parameters:
        data: pandas dataframe (subset of the test set)
    Returns:
        bokeh tab"""
    
    # Make a copy of the df
    data = data.copy()
    
    class_names = ['0', '1']
    # use specific color for each value of the target
    class_color = ['blue', 'orange']
    
    def make_request(input_data):
    	"""Function that sends html post request to the api and returns the result"""
    	#url = 'http://127.0.0.1:5000/predict/'
    	url = 'https://oc-credit-scoring.herokuapp.com/predict/'
    	r = requests.post(url=url, data=input_data)
    	try: # make sure that response is not null
    		r is not None
    	except:  # re-post the request
    		return make_request(input_data)
    	#content = r.json()
    	return r
    
    def make_dataset(entry):
        """Create column data source for the figure
        Parameters:
            entry: string, indicates the row index of the entry
        Returns: two column data sources"""
        
        idx = int(entry)
        # the data to post in json format
        input_data = data[data['SK_ID_CURR']==idx].to_json(orient='index')
        
        # Request the prediction on api server
        r = make_request(input_data)
        content = r.json()
        
        #class_names = content['result']['class names']
        explain = content['result']['explaination']
        probability = content['result']['probability']
        
        tab = pd.Series(explain, index=explain.keys())
        
        # first data source with the predicted probability
        src_pred = ColumnDataSource(data={'color':class_color, 'pred':np.round(probability, decimals=2), 'target':class_names})
        
        # 2nd data source with the interpretation of the result
        right = list(tab.values)
        y = list(tab.index)
        
        # color the negative contributions in red and positive ones in green
        color = tab.apply(lambda x: 'green' if x<0 else 'red').values
        
        src_exp = ColumnDataSource({'right':right, 'y':y, 'color':color})
        
        return src_pred, src_exp
    
    #
    def make_plot(src_pred):
        """Draw an horizontal bar of the predicted probability"""
        factors_pred = src_pred.data['target']
        p = figure(plot_width=700, plot_height=200,x_range=(0,1.15),tools='reset', y_range=factors_pred)
        p.hbar(y='target',height=0.5, left=0, right='pred', source=src_pred, color='color')
        p.title.text = "Result"
        p.title.text_font_size = "12pt"
        p.xaxis.axis_label = 'prediction probability'
        p.yaxis.axis_label = 'target'
        labels = LabelSet(x='pred', y='target', text='pred', level='glyph', x_offset=5, y_offset=-5,text_alpha=0.8, text_font_size='8pt', source=src_pred)
        p.add_layout(labels)
        p.outline_line_color = None
        p.ygrid.grid_line_color = None
        p.xgrid.grid_line_color = None
        return p
    
    #
    def make_plot_new(src_exp):
        """Draw an horizontal bar of the interpretation"""
        factors_exp = src_exp.data['y']
        TOOLTIPS = [('index', '$index'),('value','$x')]
        p = figure(plot_width=800, plot_height=400, y_range=factors_exp, tooltips=TOOLTIPS)
        p.hbar(y='y', height=0.5, left=0, right='right', color='color', source = src_exp)
        p.title.text = "Interpretation"
        p.title.text_font_size = "12pt"
        p.xaxis.axis_label = 'proportion'
        p.yaxis.axis_label = 'features'
        return p
    
    #
    def update_plot(attr, old, new):
        """update the hbars according the new entry"""
        entry_new = entry_select.value
        src_pred_new, src_exp_new = make_dataset(entry_new)
        src_pred.data.update(src_pred_new.data)
        src_exp.data.update(src_exp_new.data)
        
    # controls options
    default_entry = '274492'
    entry_options = [str(i) for i in data['SK_ID_CURR']]
    entry_select = Select(title='Entry ID', value=default_entry, options=entry_options)
    entry_select.on_change('value', update_plot)
    
    src_pred, src_exp = make_dataset(entry=default_entry)
    
    p_pred = make_plot(src_pred)
    p_exp = make_plot_new(src_exp)
    
    # put the two hbars in a single column
    plot = column(p_pred, p_exp)
    
    # create a layout
    layout = row(entry_select, plot)
    
    # Title of the panel
    predict_title = PreText(text='Predicion',style={'font-size':'16pt', 'font-weight': 'bold'})
    
    tab = Panel(child=column(predict_title, layout), title='predict-api')
    
    return tab

