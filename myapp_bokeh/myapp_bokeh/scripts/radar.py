####################################################"
# script for the radar plot

# Pandas for data management
import pandas as pd
import numpy as np


# bokeh basics
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, LabelSet, Select
from bokeh.models import Panel, PreText
from bokeh.models.glyphs import Patch
from bokeh.layouts import row, column

#----------------------------
# Print table of descriptive results
# table script

def radar_tab(df, selected_columns):
    """Draw a radar plot of the descriptive values of an entry with respect to the mean value of all the entries or a subset of the entries
    Parameters: df(pandas dataframe), selected_columns(list of strings)
    Returns: bokeh Tab object"""
    
    # copy the numeric columns of the df
    data = df.copy()
    selected_features = selected_columns
    
    # exclude the target column
    if 'TARGET' in selected_features:
        selected_features.remove('TARGET')
    data['TARGET'] = df['TARGET']
    
    if 'SK_ID_CURR' in selected_features:
        selected_features.remove('SK_ID_CURR')
    data['SK_ID_CURR'] = df['SK_ID_CURR']
    
    # or define a specific column
    # limit to 9 numeric only columns
    if len(selected_features) > 9:
        selected_features = ['AMT_ANNUITY', 'AMT_CREDIT', 'AMT_INCOME_TOTAL', 'CNT_CHILDREN', 'DAYS_BIRTH', 
                             'DAYS_EMPLOYED', 'EXT_SOURCE_2']
    
    # number of the variables
    num_vars = len(selected_features)
    
    # append the gender columns for the grouping selection
    data['CODE_GENDER'] = data['CODE_GENDER=F'].apply(lambda x : 'F' if x==1 else 'M')
    #data['DAYS_EMPLOYED'] = data['DAYS_EMPLOYED']/-365
    data['DAYS_BIRTH'] = data['DAYS_BIRTH']/-365
    
    # normalized to scale to the radar (value between 0 and 1
    normalized_df = (data[selected_features]-data[selected_features].min())/(data[selected_features].max()-data[selected_features].min())
    
    theta = np.linspace(0, 2*np.pi, num_vars, endpoint=False)
    # rotate theta such that the first axis is at the top
    theta += np.pi/2
    
    def radar_patch(r, theta=theta, centre=0.5):
        """ Returns the x and y coordinates corresponding to the magnitudes of 
    each variable displayed in the radar plot with r the radius of the circle"""
        
        # offset from centre of circle
        offset = 0.01
        yt = (r*centre + offset) * np.sin(theta) + centre
        xt = (r*centre + offset) * np.cos(theta) + centre 
        return xt, yt
    
    def make_dataset(radius, theta=theta, selected_features=selected_features, init=True):
        """create the data source with the given parameters
        Parameters: 
            init(bool): indicates if it is the first run of the function as when the drawing the frame 0.5 and 1 should be append to the coordinates"""
        
        x0, y0, r = 0.5, 0.5, radius
        verts = [(r*np.cos(t) + x0, r*np.sin(t) + y0) for t in theta]
        x = [v[0] for v in verts] 
        y = [v[1] for v in verts]
        if init:
            text = selected_features.copy()
            text.append(" ")
            src = ColumnDataSource({'x':x + [0.5],'y':y + [1],'text':text})
        else:
            text = selected_features.copy()
            src = ColumnDataSource({'x':x, 'y':y, 'text':text})
        return src
    
    def make_plot(theta=theta):
        """Draw the frame for the radar plot"""
        p = figure(title="Radar plot")
        
        # set init to true when running for the first time
        src = make_dataset(radius=0.5, init=True)
        
        p.line(x="x", y="y", source=src)
        
        # Draw 4 circle for each radius
        radius = [0.1, 0.2, 0.3, 0.4]
        # generate grid value for the radar using the maxima of each column
        max_values = data[selected_features].max()
        i = 1
        for r in radius:
            bound = ((max_values*i)//4).astype('int').values
            text = [str(i) for i in bound]
            src_new = make_dataset(radius=r,selected_features=text,init=False)
            p.text(x='x', y='y', text='text',source=src_new,
                   text_font_size="12pt", text_align="left", 
                   text_baseline="middle")
            p.circle(0.5, 0.5, radius=r, fill_color=None, line_color="lightgray")
            i+=1
        
        # radial axes
        for t in theta:
            p.ray(x=0.5, y=0.5, length=0.5, angle=t, line_width=1, color='lightgray')
        
        # label the plot with feature names
        labels = LabelSet(x="x",y="y",text="text",source=src,text_font_size='14pt', text_font='bold')
        p.add_layout(labels)
        
        return p
    
    #
    def source_radar(entry):
        """Append the coordinates of the radar in a column data source
        Parameters: entry(str)
        Returns: ColumnDataSource"""
        
        mask = data['SK_ID_CURR']==int(entry)
        idx = data.loc[mask].index 
        idx = int(idx.values)
        fidx = normalized_df.iloc[idx,:].values
        xt, yt = radar_patch(fidx)
        #grid = Patch(x=xt, y=yt, fill_alpha=0.15, fill_color='blue')
        return ColumnDataSource({'xt':xt, 'yt':yt})
    
    #
    def group_source(group):
        """Calaculate the coordinates of the mean values of the selected reference group
        Parameters: group(str)
        Returns: ColumnDataSource"""
        
        # mean of all the entries
        if group == 'ALL':
            fidx = normalized_df.mean().values
            
        # mean of the female group
        elif group == 'Female':
            tab = normalized_df.copy()
            tab['CODE_GENDER'] = data['CODE_GENDER']
            tab = tab.groupby('CODE_GENDER').agg(['mean'])
            fidx = tab.loc['F',:].values
        
        # mean of the male group
        elif group == 'Male':
            tab = normalized_df.copy()
            tab['CODE_GENDER'] = data['CODE_GENDER']
            tab = tab.groupby('CODE_GENDER').agg(['mean'])
            fidx = tab.loc['M',:].values
        
        #
        elif group == '1':
            tab = normalized_df.copy()
            tab['TARGET'] = data['TARGET']
            tab = tab.groupby('TARGET').agg(['mean'])
            fidx = tab.loc[1,:].values
        
        elif group == '0':
            tab = normalized_df.copy()
            tab['TARGET'] = data['TARGET']
            tab = tab.groupby('TARGET').agg(['mean'])
            fidx = tab.loc[0,:].values
        
        # else: no group indicator is provided, just draw the entry
        else:# None
            fidx = np.linspace(0.0, 0.0, num_vars)
        
        xt, yt = radar_patch(fidx)
        return ColumnDataSource({'xt':xt, 'yt':yt})
        
    #
    #def plot_radar(entry):
    #    p = make_plot()
    #    grid_source = source_radar(entry)
    #    glyph = Patch(x='xt', y='yt', fill_alpha=0.15, fill_color='blue')
    #    p.add_glyph(grid_source, glyph)
    #    return p
    
    #
    def update_entry(attr, old, new):
        """update the plot according to the new entry"""
        #update_radar()
        entry = entry_select.value    
        new_entry_src = source_radar(entry)
        entry_src.data.update(new_entry_src.data)

    #
    def update_group(attr, old, new):
        #update_radar()
        group = group_select.value
        new_group_src = group_source(group)
        group_src.data.update(new_group_src.data)

    #def update_radar(selected=None):
    #    entry = entry_select.value
    #    group = group_select.value
    #
    #    new_entry_src = source_radar(entry)
    #    new_group_src = group_source(group)
    #
    #    entry_src.data.update(new_entry_src.data)
    #    group_src.data.update(new_group_src.data)


    # Define the controls
    entry_options = [str(i) for i in data['SK_ID_CURR']]
    default_entry = '274492'
    entry_select = Select(title='Entry ID', value=default_entry, options=entry_options)
    
    group_select = Select(title='Group', value='None', options=['ALL', 'Female', 'Male', '1', '0', 'None'])

    # make the plot
    p = make_plot()
    
    # add legends
    p.circle([0.01, 0.01], [0.0, 0.05], color=['red','royalblue'], radius=0.01)
    p.text([0.02, 0.02], [0.0, 0.05], text=['group','entry'],
       text_font_size="10pt", text_align="left", text_baseline="middle")
    
    # source data
    entry_src = source_radar(entry=default_entry)
    group_src = group_source(group='None')
    
    p.patch(x='xt', y='yt',source=entry_src, fill_alpha=0.15, fill_color='blue')
    p.patch(x='xt', y='yt',source=group_src, fill_alpha=0.15, fill_color='red')
    
    entry_select.on_change('value', update_entry)
    group_select.on_change('value', update_group)

    # put controls in a single column
    controls = column(entry_select, group_select)
    
    # create a layout
    layout = row(controls, p)
    
    # put the layout in a tab
    tab_title = PreText(text='Radar plot',style={'font-size':'18pt', 'font-weight': 'bold'})
    
    tab = Panel(child=column(tab_title, layout), title='radar plot')
    return tab