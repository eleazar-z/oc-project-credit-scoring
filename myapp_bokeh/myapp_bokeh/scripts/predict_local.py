# prediction and interpretation tab script
# This script uses local host stored objects

# basic python libraries
import numpy as np
import pandas as pd

# bokeh libraries
from bokeh.models import LabelSet, ColumnDataSource
from bokeh.plotting import figure
from bokeh.io import curdoc
from bokeh.layouts import row, column
from bokeh.models import Select, Panel, PreText

# Interpretability of the predictions
import lime
import lime.lime_tabular


def predict_tab_2(data, model):
    """Draw horizontals bars of the predicted probability and the interpretation of the output (contribution of some features) based on provided imput
    Parameters:
        data: pandas dataframe (subset of the test set)
        model: dictionay containing a classifier object trained on similar data
                and other resources
    Returns:
        bokeh tab"""
    
    # Make sure that there are not missing data as lime does not support null values
    # or use an imputer
    #data = data.dropna(how='any').copy()
    data = data.copy()
    
    # Assign the pickle objects
    df_columns = model['columns']
    feature_names = list(df_columns.columns)
    lr_model = model['model']
    X_train = model['X_train']
    imputer = model['imputer']
    scaler = model['scaler']
    
    class_names = ['0', '1']
    class_color = ['green', 'red']
    
    # Align with the columns used to train the model
    data_align, _ = data.align(df_columns, join='right', axis=1)
    
    # Apply the same imputer and scaler functions before predicting
    X_test = scaler.transform(imputer.transform(data_align))
    
    # load the lime expaliner object
    # The lime explainer object
    explainer = lime.lime_tabular.LimeTabularExplainer(X_train, feature_names=feature_names,
                                                       class_names=class_names, 
                                                       kernel_width=3)
    
    def make_dataset(entry):
        """Create column data source for the figure
        Parameters:
            entry: string, indicates the row index of the entry
        Returns: two column data sources"""
        
        idx = data[data['SK_ID_CURR']==int(entry)].index.values.astype(int)[0]
        pred = lr_model.predict_proba(X_test[[idx]])
        exp = explainer.explain_instance(X_test[idx], lr_model.predict_proba, num_features=6)
        exp_list = exp.as_list()
        tab = pd.Series(dict(exp_list), index=dict(exp_list).keys())
        
        # first data source with the predicted probability
        src_pred = ColumnDataSource(data={'color':class_color, 'pred':np.round(list(pred[0]), decimals=2), 'target':class_names})
        
        # 2nd data source with the interpretation of the result
        right = list(tab.values)
        y = list(tab.index)
        
        # color the negative contributions in red and positive ones in green
        color = tab.apply(lambda x: 'green' if x<0 else 'red').values
        
        src_exp = ColumnDataSource({'right':right, 'y':y, 'color':color})
        
        return src_pred, src_exp
    
    #
    def make_plot(src_pred):
        """Draw an horizontal bar of the predicted probability"""
        factors_pred = src_pred.data['target']
        p = figure(plot_width=700, plot_height=200,x_range=(0,1.15),tools='reset', y_range=factors_pred)
        p.hbar(y='target',height=0.5, left=0, right='pred', source=src_pred, color='color')
        p.title.text = "Result"
        p.title.text_font_size = "12pt"
        p.xaxis.axis_label = 'prediction probability'
        p.yaxis.axis_label = 'target'
        labels = LabelSet(x='pred', y='target', text='pred', level='glyph', x_offset=5, y_offset=-5,text_alpha=0.8, text_font_size='8pt', source=src_pred)
        p.add_layout(labels)
        p.outline_line_color = None
        p.ygrid.grid_line_color = None
        p.xgrid.grid_line_color = None
        return p
    
    #
    def make_plot_new(src_exp):
        """Draw an horizontal bar of the interpretation"""
        factors_exp = src_exp.data['y']
        TOOLTIPS = [('index', '$index'),('value','$x')]
        p = figure(plot_width=800, plot_height=400, y_range=factors_exp, tooltips=TOOLTIPS)
        p.hbar(y='y', height=0.5, left=0, right='right', color='color', source = src_exp)
        p.title.text = "Interpretation"
        p.title.text_font_size = "12pt"
        p.xaxis.axis_label = 'proportion'
        p.yaxis.axis_label = 'features'
        return p
    
    #
    def update_plot(attr, old, new):
        """update the hbars according the new entry"""
        entry_new = entry_select.value
        src_pred_new, src_exp_new = make_dataset(entry_new)
        src_pred.data.update(src_pred_new.data)
        src_exp.data.update(src_exp_new.data)
        
    # controls options
    default_entry = '274492'
    entry_options = [str(i) for i in data['SK_ID_CURR']]
    entry_select = Select(title='Entry ID', value=default_entry, options=entry_options)
    entry_select.on_change('value', update_plot)
    
    src_pred, src_exp = make_dataset(entry=default_entry)
    
    p_pred = make_plot(src_pred)
    p_exp = make_plot_new(src_exp)
    
    # put the two hbars in a single column
    plot = column(p_pred, p_exp)
    
    # create a layout
    layout = row(entry_select, plot)
    
    # Title of the panel
    predict_title = PreText(text='Predicion',style={'font-size':'16pt', 'font-weight': 'bold'})
    
    tab = Panel(child=column(predict_title, layout), title='predict')
    
    return tab

