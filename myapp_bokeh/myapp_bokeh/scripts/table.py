# descriptive statistic table script

# Pandas for data management
import pandas as pd

# bokeh packages
from bokeh.models import Select, ColumnDataSource, Panel, PreText
from bokeh.layouts import row, column
from bokeh.models.widgets import TableColumn, DataTable


def table_tab(df, selected_features):
    """Calculate descriptive statistics of all the columns or a given for all the entries or a group of entries
    Parameters:
            df: pandas dataframe
            selected_features: list
    Returns:
        tab: bokeh tab"""
    #
    def tabulate(data, var, group):
        """Calculate the descriptive statistics
        Parameters:
            data: dataframe
            var: string indicating the column
            group: string indicating the grouping column
        Returns:
            bokeh: ColumnDataSource"""
        
        # calculate for the whole sample and all the variables
        if group =='ALL' and var=='ALL':
            stats = data.loc[:,~data.columns.isin(['SK_ID_CURR', 'TARGET'])].describe()
        
        # all the entries and a selected variable
        elif group =='ALL' and var!='ALL':
            stats = data[var].describe()
            stats = stats.reset_index().rename(columns={'index':'statistics'})
        
        # a specific group and all the variables
        elif group !='ALL' and var=='ALL':
            stats = data.loc[:,~data.columns.isin(['SK_ID_CURR'])].groupby(group).describe()
            # Present only mean; possible to add other statistics
            stats = stats.loc[:,stats.columns.get_level_values(1).isin(['mean'])]
            stats = stats.T.reset_index().rename(columns={'level_0':'variables', 'level_1':'statistics'})
        
        # a specific group and a specific variable
        else:
            stats = data.groupby(group)[var].describe()
            stats = stats.rename(columns={'50%':'median'})
            stats = stats.T
        
        # Round the value to two
        stats = stats.round(2)
        stats = pd.DataFrame(stats).rename(columns=str)
        return ColumnDataSource(stats)
    
    #
    def make_table(src):
        """Generate the bokeh table object with a given data source"""
        table = DataTable(source=src, columns=[TableColumn(field=c, title=c) for c in src.column_names], width=800)
        return table
    
    # 
    def nix(var, lst1, lst2):
        """Refine the selection options in order to not have the same column in the two (grouping and stat columns)
        Parameters:
            var: string, indicates the selected variable to be excluded
            lst1: list, indicate all the default options
            lst2: list, output option and exclude var if present
        Returns:
            list"""
        lvar = [x for x in lst1 if (x in lst2 and x!=var)]
        # append 'ALL' option to give calculate for all the entries
        lvar.append('ALL')
        return lvar
    #
    def update_variable(attr, old, new):
        """update the table following the new variable"""
        group_select.options = nix(new, all_options, group_options)
        update_table()
    
    #
    def update_group(attr, old, new):
        """update the table following the new grouping option"""
        variable_select.options = nix(new, all_options, var_options)
        update_table()
    #
    def update_table(selected=None):
        """updata the dscriptibe stats table"""
        var_new = variable_select.value
        group_new = group_select.value
        src_new = tabulate(data, var_new, group_new)
        #source.data.update(source_new.data)
        #table = make_table(source_new)
        layout.children[1] = make_table(src_new)
        
    # -----------------------------------------
    # Initialize the table
    
    # copy the data
    #data = df.select_dtypes('number').copy()
    data = df[selected_features].copy()
    
    # Transform number of days in year
    #data['DAYS_EMPLOYED'] = df['DAYS_EMPLOYED']/-365
    data['DAYS_BIRTH'] = df['DAYS_BIRTH']/-365
    
    # append the gender columns for the grouping selection
    data['CODE_GENDER'] = df['CODE_GENDER=F'].apply(lambda x : 'F' if x==1 else 'M')
    
    # the selection of variables to tabulate
    all_options = sorted(data.columns)
    var_options = [elt for elt in all_options if elt not in ['SK_ID_CURR', 'TARGET']]
    # restrict the grouping columns
    group_options = ['CODE_GENDER', 'TARGET']
    
    # create controls
    variable_select = Select(title='Variable', value='AMT_INCOME_TOTAL', options=nix('CODE_GENDER', all_options, var_options))
    group_select = Select(title='Groupe', value='ALL', options=nix('AMT_INCOME_TOTAL', all_options, group_options))
    
    # generate the table
    src = tabulate(data, var='AMT_CREDIT', group='ALL')
    table = make_table(src)
    
    # update controls
    #for w in [variable_select, group_select]:
    #    w.on_change('value', update_table)
    variable_select.on_change('value', update_variable)
    group_select.on_change('value', update_group)
    
    # define a title for the tab
    table_title = PreText(text='Descriptive statistics',style={'font-size':'16pt', 'font-weight': 'bold'}) 
    
    # put the controls in a single column
    controls = column(variable_select, group_select, width=200)
    layout = row(controls, table)
    
    # Make a tab for the output
    tab = Panel(child=column(table_title, layout), title='table')
    
    return tab