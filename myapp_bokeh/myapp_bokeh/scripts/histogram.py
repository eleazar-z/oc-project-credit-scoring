import pandas as pd
import numpy as np

# bokeh basics
from bokeh.plotting import figure
from bokeh.models import Panel, PreText, ColumnDataSource
from bokeh.models import CDSView, IndexFilter, Select
from bokeh.layouts import row, column


def histogram_tab(df, selected_columns):
    """Plot the distribution histogram of a numeric column
    Parameters: df, pandas dataframe; selected_columns, list of column names
    Returns: bokeh tab"""
    
    #
    def make_dataset(data, col):
        """Create a ColumnDataSource with the histogram values (classes and count) of the column
        Parameters:
            data(pandas dataframe), col(strin) indicate the column name
        Returns: ColumnDataSource"""
        
        data_hist, edges = np.histogram(data[col], bins=int(data.shape[0]/5))
        
        # put the info in a dataframe
        df_hist = pd.DataFrame({'count':data_hist, 'left':edges[:-1], 'right': edges[1:]})
        # Divide by the total
        df_hist['proportion'] = df_hist['count']/df_hist['count'].sum()
        
        return ColumnDataSource(df_hist)
    
    #
    def make_view_filter(data, entry, col):
        """Define a filter and return the corresponding class of a given entry
        Parameters:
            data(pandas dataframe)
            entry: one value of the column chosen as index column ('SK_ID_CURR' in this case)
            col(string) identical to the one used in make_dataset
        Returns: index (int)"""
        
        idx_value = data[data['SK_ID_CURR']==int(entry)][col]
        idx_value = idx_value.iat[0]
        data_hist, edges = np.histogram(data[col], bins=int(data.shape[0]/5))
        
        # put the info in a dataframe
        df_hist = pd.DataFrame({'count':data_hist, 'left':edges[:-1], 'right': edges[1:]})
        
        # create mask to filter accrording to the class of the entry
        mask1 = (idx_value > df_hist['left']) 
        mask2 = (idx_value < df_hist['right'])
        
        # return the index of the class in the histogram table
        idx = df_hist.index.get_loc(df_hist.index[(mask1) & (mask2)][0])
        
        return idx
    
    #
    def update_entry(attr, old, new):
        """update the histogram base on the new value of the entry"""
        update_hist()    
    #
    def update_col(attr, old, new):
        """update the histogram base on the selected column"""
        update_hist()
    #
    def update_hist(selected=None):
        """general update function"""
        col_new = col_select.value
        entry_new = entry_select.value
        
        # update the figure labels
        p.xaxis.axis_label = str(col_new)
        p.title.text = ('Distribution of {} with hover on client {}'.format(str(col_new), str(entry_new)))
        
        # update the source data with the new values
        src_new = make_dataset(data, col_new)    
        src.data.update(src_new.data)
        
        # refine the filter
        idx_new = make_view_filter(data, entry_new, col_new)
        view.filters = [IndexFilter([idx_new])]

    #-----------------------------------
    # copy only the numeric columns of the df
    #data = df.select_dtypes('number').copy()
    data = df[selected_columns].copy()
    
    # Transform number of days in year
    #data['DAYS_EMPLOYED'] = df['DAYS_EMPLOYED']/365
    data['DAYS_BIRTH'] = df['DAYS_BIRTH']/-365
    
    # make sure that the identifier column has been selected
    if 'SK_ID_CURR' not in data.columns:
        data['SK_ID_CURR'] = df['SK_ID_CURR'].values
    
    # Set the filter options to all the values of the 'SK_ID_CURR' column
    # convert in str as bokeh does not support other object for the Select
    entry_options = [str(i) for i in data['SK_ID_CURR']]
    
    # load the figure
    default_entry = str(274492)  # chose an entry for the first run
    default_col = 'AMT_INCOME_TOTAL'
    
    tools = ['box_select', 'hover', 'reset']
    p = figure(plot_height=600, plot_width=600, tools=tools)
    
    # the figure labels
    p.title.text = 'Distribution of {} with hover on client {}'.format('AMT_INCOME_TOTAL', default_entry)
    p.xaxis.axis_label = default_col
    p.yaxis.axis_label = 'proportion'
    
    # the source data
    src = make_dataset(data, col=default_col)
    
    # index of the filter
    idx = make_view_filter(data, entry=default_entry, col=default_col)
    view = CDSView(source=src, filters=[IndexFilter([idx])])
    
    # draw the histogram
    p.quad(bottom=0, top='proportion', left='left',
       right='right', source=src, fill_color='blue', line_color='black')
    
    # draw the hover with the filter
    p.quad(bottom=0, top='proportion', left='left', right='right', source=src,view=view, hover_color='orange', fill_color='red', line_color='black')
    
    # define the controls
        # all the values of the 'SK_ID_CURR' column
    entry_select = Select(title='Entry ID', value=default_entry, options=entry_options)
    entry_select.on_change('value', update_entry)
    
    # select numeric columns
    col_options = [col for col in sorted(data.columns) if col!='SK_ID_CURR']
    col_select = Select(title='Variable', value=default_col, options=col_options)
    col_select.on_change('value', update_col)
    
    # put controls in a single column
    controls = column(col_select, entry_select)
    
    # create a layout
    layout = row(controls, p)
    
    # make a tab with the layout
        # add a title
    tab_title = PreText(text='Histogram',style={'font-size':'18pt', 'font-weight': 'bold'})
    
    tab = Panel(child=column(tab_title, layout), title='histogram')
    
    return tab