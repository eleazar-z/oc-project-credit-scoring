########################################################################
##### Add-on Function Modules for Project #7 ########
#############################################################

# Import required packages
import gc
import numpy as np
import pandas as pd


# lower case of column names
def lower_case_columns(df):
    """Lowercase the column names"""
    df.rename(columns=str.lower, inplace=True)
    return df


# Convert data types in order to reduce memory usage
def convert_types(df, print_info=True):
    """This function helps reduce memory usage by using more efficient types for the variables. 
    For example category is often a better type than object (unless the number of unique 
    categories is close to the number of rows in the dataframe).
    PARAMETERS:
        df: A dataframe
        print_info: bool
            if memory usage informations should be printed at the end
    RETURNS:
        a dataframe"""
    
    original_memory = df.memory_usage().sum()
    
    # Iterate through each column
    for c in df:
        
        # Convert ids and booleans to integers
        if ('SK_ID' in c):
            df[c] = df[c].fillna(0).astype(np.int32)
            
        # Convert objects to category
        elif (df[c].dtype == 'object') and (df[c].nunique() < df.shape[0]):
            df[c] = df[c].astype('category')
        
        # Booleans mapped to integers  
        #elif list(df[c].unique()) == [1, 0]:  # uncomment to set boolean objects
        #    df[c] = df[c].astype(bool)
        
        # Float64 to float32
        elif df[c].dtype == float:
            df[c] = df[c].astype(np.float32)
            
        # Int64 to int32
        elif df[c].dtype == int:
            df[c] = df[c].astype(np.int32)
        
    new_memory = df.memory_usage().sum()
    
    if print_info:
        print(f'Original Memory Usage: {round(original_memory / 1e9, 3)} gb.')
        print(f'New Memory Usage: {round(new_memory / 1e9, 3)} gb.')
        
    return df

# Retrive information about data types and missing values by rows and columns
def missing_values_table(dataframe, axis=0, sort=True):
    """Function that summarizes the main information about the columns in the dataset.
    Mainly the dtype of each column, number of data present and missing.
    PARAMETERS
        dataframe: pandas.DataFrame
        axis: 0 to apply the function by columns and 1 by rows
    RETURN
        pandas.DataFrame."""
    if axis==0:
        tab_recap=pd.DataFrame({"column_dtype":dataframe.dtypes})
        tab_recap["value_count"]=pd.Series(dataframe.apply(lambda x: x.size, axis=0))
    else:
        tab_recap=pd.DataFrame({"value_count":dataframe.apply(lambda x: x.size, axis=1)})
    tab_recap["filling_factor"]=pd.Series(dataframe.count(axis=axis))
    tab_recap["missing_value"]=pd.Series(dataframe.apply(lambda x: x.isnull().sum(), axis=axis))
    tab_recap["missing_value_ratio"]=pd.Series(dataframe.apply(lambda x:round((((x.isnull().sum())*100)/x.size),3), axis=axis))
    if sort==True:
        return tab_recap.sort_values('missing_value_ratio', ascending=False)
    else:
        return tab_recap
    
# Remove columns or rows with missing values above a threshold
def remove_missing_columns(dataframe, threshold=0.90, axis=0):
    """Function that removes columns where the lines have a proportion of
    missing data greater than or equal to the threshold provided.
    Parameters: 
        dataframe: pandas.DataFrame
        axis (int): default is 0 (column) 1(row)
        threshold(float) : default is 0.90, 
    Returns:
        pd.DataFrame: pandas data frame"""
    df_null=pd.DataFrame({"ratio":dataframe.apply(lambda x:x.isnull().sum()/x.size, axis=axis)})
    df_null=df_null[df_null["ratio"]<threshold]
    masque=df_null.index
    if axis==0:
        dataframe=dataframe[masque]
        print("{} columns have been removed".format(len(masque)))
    else:
        dataframe=dataframe.loc[masque]
        print("{} rows have been removed".format(len(masque)))
    return dataframe

# Aggregation functions
    # Different functions were retrieved from an existing kaggle kernel to prepare the data for the modelisaion.
    # Here are the links to the kernel:
        # https://www.kaggle.com/c/home-credit-default-risk/data
        # https://www.kaggle.com/willkoehrsen/start-here-a-gentle-introduction
        # https://www.kaggle.com/willkoehrsen/introduction-to-manual-feature-engineering
        # https://www.kaggle.com/willkoehrsen/introduction-to-manual-feature-engineering-p2
#
# Function for numeric aggregations
def agg_numeric(df, group_var, df_name):
    """Aggregates the numeric values in a dataframe. This can
    be used to create features for each instance of the grouping variable.
    
    Parameters
    --------
        df (dataframe): 
            the dataframe to calculate the statistics on
        group_var (string): 
            the variable by which to group df
        df_name (string): 
            the variable used to rename the columns
        
    Return
    --------
        agg (dataframe): 
            a dataframe with the statistics aggregated for 
            all numeric columns. Each instance of the grouping variable will have 
            the statistics (mean, min, max, sum; currently supported) calculated. 
            The columns are also renamed to keep track of features created.
            Columns with all duplicate values are removed.     
    """
    # Remove id variables other than grouping variable
    for col in df:
        if col != group_var and 'SK_ID' in col:
            df = df.drop(columns = col)
            
    group_ids = df[group_var]
    numeric_df = df.select_dtypes('number').copy()
    numeric_df[group_var] = group_ids

    # Group by the specified variable and calculate the statistics
    agg = numeric_df.groupby(group_var).agg(['count', 'mean', 'max', 'min', 'sum']).reset_index()

    # Need to create new column names
    columns = [group_var]

    # Iterate through the variables names
    for var in agg.columns.levels[0]:
        # Skip the grouping variable
        if var != group_var:
            # Iterate through the stat names
            for stat in agg.columns.levels[1][:-1]:
                # Make a new column name for the variable and stat
                columns.append('%s_%s_%s' % (df_name, var, stat))

    agg.columns = columns
    
    # Remove the columns with all redundant values
    _, idx = np.unique(agg, axis=1, return_index=True)
    agg = agg.iloc[:, idx]
    
    return agg

# Function to handle categorical variables
def agg_categorical(df, group_var, df_name, functions=['mean']):
    """Computes counts and normalized counts for each observation
    of `group_var` of each unique category in every categorical variable
    
    Parameters
    --------
    df : dataframe 
        The dataframe to calculate the value counts for.
        
    group_var : string
        The variable by which to group the dataframe. For each unique
        value of this variable, the final dataframe will have one row
        
    df_name : string
        Variable added to the front of column names to keep track of columns
        
    function: list of string
        Variable added in order to specify the operation (could select both or one of them)
        By default it is ['mean']. Could either ['sum', 'mean'] or ['sum', 'count', 'mean']
    
    Return
    --------
    categorical : dataframe
        A dataframe with counts and normalized counts of each unique category in every 
        categorical variable with one row for every unique value of the `group_var`.
    """
    
    # Select the categorical columns
    categorical = pd.get_dummies(df.select_dtypes('category'))

    # Make sure to put the identifying id on the column
    categorical[group_var] = df[group_var]

    # Groupby the group var and calculate the sum and mean
    categorical = categorical.groupby(group_var).agg(functions)
    
    column_names = []
    
    # Iterate through the columns in level 0
    for var in categorical.columns.levels[0]:
        # Iterate through the stats in level 1
        for stat in functions:
            # Make a new column name
            column_names.append('%s_%s_%s' % (df_name, var, stat))
    
    categorical.columns = column_names
    
    # Remove duplicate columns by values
    _, idx = np.unique(categorical, axis=1, return_index=True)
    categorical = categorical.iloc[:, idx]
    
    return categorical

# Function to Aggregate Stats at the Client Level
def aggregate_client(df, group_vars, df_names):
    """Aggregate a dataframe with data at the loan level at the client level
    
    Args:
        df (dataframe): data at the loan level
        group_vars (list of two strings): grouping variables for the loan 
        and then the client (example ['SK_ID_PREV', 'SK_ID_CURR'])
        names (list of two strings): names to call the resulting columns
        (example ['cash', 'client'])
        
    Returns:
        df_client (dataframe): aggregated numeric stats at the client level. 
        Each client will have a single row with all the numeric data aggregated
    """
    
    # if data are already grouped by loan
    if len(np.unique(df[group_vars[0]])) == df.shape[0]:
        
        # Aggregate the numeric columns
        df_agg = agg_numeric(df, group_var=group_vars[1], df_name=df_names[1])
        
        # If there are categorical variables
        if any(df.dtypes=='category'):
            
            # Count the categorical columns
            df_counts = agg_categorical(df, group_var=group_vars[1], df_name=df_names[1])     
            
            # Merge the values generate df_by_client
            df_by_client = df_counts.merge(df_agg, on=group_vars[1], how='left')
        
        # No categorical variables
        else:
            df_by_client = df_agg
    
    # Aggregrate by loan first and then by client
    else: 
        
        # Aggregate the numeric columns
        df_agg = agg_numeric(df, group_var=group_vars[0], df_name=df_names[0])
        
        # If there are categorical variables
        if any(df.dtypes=='category'):
            
            # Count the categorical columns
            df_counts = agg_categorical(df, group_var=group_vars[0], df_name=df_names[0])
            
            # Merge the numeric and categorical
            df_by_loan = df_counts.merge(df_agg, on=group_vars[0], how='outer')
            
            # Delete df_agg and df_count and manage memory
            gc.enable()
            del df_agg, df_counts
            gc.collect()
        # No categorical variables
        else:
            # consider only numeric columns in dataframe
            df_by_loan = df_agg.copy()
            
            # Delete df_agg
            gc.enable()
            del df_agg
            gc.collect()
        
        # Merge to get the client id in dataframe
        df_by_loan = df_by_loan.merge(df[[group_vars[0], group_vars[1]]], on=group_vars[0], 
                                      how='left')
        
        # Remove the loan id
        df_by_loan = df_by_loan.drop(columns=[group_vars[0]])
        
        # Aggregate numeric stats by column
        df_by_client = agg_numeric(df_by_loan, group_var = group_vars[1], df_name = df_names[1])
        
        # Memory management
        gc.enable()
        del df, df_by_loan
        gc.collect()

    return df_by_client

# Retrieve highly correlated columns and remove one of them
def correlated_variables(df, threshold=0.85, remove=False):
    """
    Drops features that are strongly correlated to other features.
    This lowers model complexity, and aids in generalizing the model.
    Parameters:
        df(dataframe): features df (x)
        threshold(float): Columns are dropped relative to the threshold input (e.g. 0.85)
        remove(bool): if the correlated columns should be dropped from the df
    Returns:
        Print the number of correlated variables to remove
        Output(dataframe): df that only includes uncorrelated features
    """
    # Create correlation matrix
    corr_matrix = df.corr().abs()
    
    # Select upper triangle of correlation matrix
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
    
    # Find features with correlation greater than threshold
    to_drop = [column for column in upper.columns if any(upper[column] > threshold)]
    
    # Remove the target column if present in the list
    if 'TARGET' in to_drop:
        to_drop.remove('TARGET')
    
    # Drop features
    if remove==True:
        df.drop(to_drop, axis=1, inplace=True)
        print('%d columns were dropped from the dataframe.' % (len(to_drop)))
        return df
    else:
        print('There are %d columns to remove.' % (len(to_drop)))
        
# Select columns with correlations above threshold
def correlated_variables(df, threshold=0.85, remove=False):
    """
    Drops features that are strongly correlated to other features.
    This lowers model complexity, and aids in generalizing the model.
    Parameters:
        df(dataframe): features df (x)
        threshold(float): Columns are dropped relative to the threshold input (e.g. 0.85)
        remove(bool): if the correlated columns should be dropped from the df
    Returns:
        Print the number of correlated variables to remove
        Output(dataframe): df that only includes uncorrelated features
    """
    # Create correlation matrix
    corr_matrix = df.corr().abs()
    
    # Select upper triangle of correlation matrix
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
    
    # Find features with correlation greater than threshold
    to_drop = [column for column in upper.columns if any(upper[column] > threshold)]
    
    # Remove the target column if present in the list
    if 'TARGET' in to_drop:
        to_drop.remove('TARGET')
    
    # Drop features
    if remove==True:
        df.drop(to_drop, axis=1, inplace=True)
        print('%d columns were dropped from the dataframe.' % (len(to_drop)))
        return df
    else:
        print('There are %d columns to remove.' % (len(to_drop)))
        
# Correlation with the target variable
def correlation_target(df, target='TARGET', threshold=0.5):
    """Display worrelation of the features with target
    and use a threshold to present a limited result
        Parameters: 
            df: dataframe
            target: string
            threshold: int or None
    Returns:
        dataframe"""
    df_corr = pd.DataFrame(df.corr()[target])
    if threshold is None:
        return df_corr
    else:
        mask = (np.absolute(df_corr[target])>threshold)
        #mask2 = df_corr[target].isnull()  # Uncomment to look into the null values
        df_corr = df_corr[mask]
        df_corr = df_corr.loc[~df_corr.index.isin([target]),:]
        no_corr = df_corr.shape[0]
        if no_corr == 0:
            print('There is no variable with correlation above {}%'.format(threshold*100))
        else:
            print('There are {} variables correlated with the {} at above {}%'.\
                  format(df_corr.shape[0], target, threshold*100))
            return df_corr
        
#
