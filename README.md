# oc-project-credit-scoring

The aim of this project is to carry out a scoring model that will give a prediction on the probability of bankruptcy of a customer automatically.  It has been carried out for a data science training program on **OpenClassrooms** (https://openclassrooms.com/fr/). To do so, we use data from a financial firm (fictitious company "*Prêt à dépenser*") that offers consumer credit for people with little or no loan history. The data are retrieved from this link: www.kaggle.com/c/home-credit-default-risk/data.

The project includes:
- two notebooks (**preprocessing** and **modelling** of the data)
- a **module** of functions written for the project
- a **flask app** code to deploy the result on an api-server (heroku)
- a **dashboard app**, built using the bokeh library, deployed on a server as well.

visit https://oc-dashboard.herokuapp.com/myapp_bokeh to take a look into the dashboard (you may need to reload the page as the server might be resuming) !
